import config from "../config";
import { request } from "services/api";

const baseEndpoint = `${config.apiBaseURL}/rooms`;

export const getAllSongs = (data) => {
	const endpoint = `${config.apiBaseURL}/music`;
	return request(endpoint, "GET");
};

export const getAllMessages = (data) => {
	const endpoint = `${baseEndpoint}/messages?roomId=${data.roomId}`;
	return request(endpoint, "GET");
};

export const getAllRooms = (data) => {
	const endpoint = `${config.apiBaseURL}/roomList`;
	return request(endpoint, "GET");
};

export const getAllNotifications = (data) => {
	const endpoint = `${config.apiBaseURL}/notify`;
	return request(endpoint, "GET");
};

export const postSeenNotifications = (data) => {
	const endpoint = `${config.apiBaseURL}/notify/seen`;
	return request(endpoint, "GET");
};

export const postSeenMessage = (data) => {
	const endpoint = `${baseEndpoint}/messages/seen?roomId=${data.roomId}`;
	return request(endpoint, "GET");
};

export const uploadFile = (data) => {
	const endpoint = `${config.apiBaseURL}/files`;
	var formData = new FormData();
	formData.append("file", data);
	return request(endpoint, "POST", formData, true);
};

export const uploadImage = (data) => {
	const endpoint = `${config.apiBaseURL}/files/image`;
	var formData = new FormData();
	formData.append("file", data);
	return request(endpoint, "POST", formData, true);
};
