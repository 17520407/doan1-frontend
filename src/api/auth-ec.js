import config from "../config";
import { request } from "services/api";
var jwt = require("jsonwebtoken");
const secretKey = "login-serect-jasjd=kaskl6463546";

const baseEndpoint = `${config.apiBaseURL}/users`;

export function login(data) {
	var token = jwt.sign(
		{
			avatar: data.imageUrl,
			email: data.email,
		},
		secretKey
	);
	const endpoint = `${baseEndpoint}/login`;
	return request(endpoint, "POST", { token });
}

export function updateUserInfo(data) {
	const endpoint = `${baseEndpoint}/login/${data._id}`;
	return request(endpoint, "POST", data);
}
export function getUserInfo(data) {
	const endpoint = `${baseEndpoint}`;
	return request(endpoint, "GET");
}
