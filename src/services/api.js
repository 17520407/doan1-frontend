import axios from "axios";
import store from "store";
import _ from "lodash";
import { history } from "../stores";
import config from "../config";
import { save, clearAll, get } from "services/localStoredService";
import socketIOClient from "socket.io-client";

export const socket = socketIOClient(`${config.apiURL}`);

export const openStream = () => {
	const config = { audio: false, video: true };
	let browerMedia = navigator.mediaDevices.getUserMedia(config);
	if (browerMedia === undefined) {
		console.log("Https is required");
	}
	return browerMedia;
};

export const refresh = (requestData, refreshToken) => {
	return axios({
		method: "GET",
		url: `${config.apiBaseURL}/users/refresh`,
		headers: {
			authorization: refreshToken,
			"Content-Type": "application/json",
		},
	})
		.then(async (res) => {
			const { accessToken } = res.data;
			save("accessToken", accessToken);
			const { endpoint, method, data, config } = requestData;
			return request(endpoint, method, data, config, accessToken);
		})
		.catch((err) => {
			window.location.reload();
			clearAll();
			return err;
		});
};

export const handleError = async (error, requestData) => {
	return new Promise(async (resolve, reject) => {
		const status = _.get(error, "response.status");
		const refreshToken = get("refreshToken");
		if (status >= 400 && status <= 500) {
			if (status === 404) {
				return history.push("/404");
			} else {
				if (status === 401) {
					if (refreshToken)
						return resolve(
							await refresh(requestData, refreshToken)
						);
					return;
				} else {
					return resolve(_.get(error, "response", ""));
				}
			}
		} else {
			return resolve(_.get(error, "response", ""));
		}
	});
};

export const CONTENT_TYPE = {
	form: "multipart/form-data",
	json: "application/json",
};

export const request = (
	endpoint,
	method,
	data,
	formData = false,
	accessToken = null
) => {
	const getHeaders = (contentType = CONTENT_TYPE.json) => {
		const token = accessToken ? accessToken : store.get("accessToken");
		const header = {
			authorization: token,
			"Content-Type": contentType,
		};
		return header;
	};

	const headers = !formData ? getHeaders() : getHeaders(CONTENT_TYPE.form);
	const options = {
		method,
		url: endpoint,
		headers,
		data: method !== "GET" ? data : null,
		params: method === "GET" ? data : null,
	};
	return axios(options).catch((error) => {
		return handleError(error, {
			endpoint,
			method,
			data,
			config,
		});
	});
};
