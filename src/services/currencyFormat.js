
export const currencyFormat = num => {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,") + " Đ";
};

export default {
  currencyFormat,
};
