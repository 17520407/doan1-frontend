/**
 * @file All actions will be listed here
 */

import { createAction } from 'redux-actions';
import * as CONST from './constants';

export const login = createAction(CONST.LOGIN);
export const loginSuccess = createAction(CONST.LOGIN_SUCCESS);
export const loginFail = createAction(CONST.LOGIN_FAIL);

export const register = createAction(CONST.REGISTER);
export const registerSuccess = createAction(CONST.REGISTER_SUCCESS);
export const registerFail = createAction(CONST.REGISTER_FAIL);

export const verifyRegister = createAction(CONST.VERIFY_REGISTER);
export const verifyRegisterSuccess = createAction(CONST.VERIFY_REGISTER_SUCCESS);
export const verifyRegisterFail = createAction(CONST.VERIFY_REGISTER_FAIL);

export const forgotPassword = createAction(CONST.FORGOT_PASSWORD);
export const forgotPasswordSuccess = createAction(CONST.FORGOT_PASSWORD_SUCCESS);
export const forgotPasswordFail = createAction(CONST.FORGOT_PASSWORD_FAIL);

export const updatePassword = createAction(CONST.UPDATE_PASSWORD);
export const updatePasswordSuccess = createAction(CONST.UPDATE_PASSWORD_SUCCESS);
export const updatePasswordFail = createAction(CONST.UPDATE_PASSWORD_FAIL);

export const notify = createAction(CONST.NOTIFY);

/*---------------------------------------------------------------------------------------------------------------------------------*/
export const toggleForm = createAction(CONST.TOGGLE_FORM);
export const toggleLoading = createAction(CONST.TOGGLE_LOADING);
export const handleClear = createAction(CONST.HANDLE_CLEAR);
export const handleSelectChange = createAction(CONST.HANDLE_SELECT_CHANGE);