import React from "react";
import { FormGroup, Input, FormFeedback } from "reactstrap";
import { useForm } from "react-hook-form";
import { urlFormat } from "services/urlFormat";

export default function App(props) {
	const { register, errors, handleSubmit } = useForm();
	const { isProcessing } = props;

	const onSubmit = (data, e) => {
		props.actions.toggleLoading(true);
		props.actions.verifyRegister({
			verifyCode: data.verifyCode.trim(),
		});
	};

	return (
		<React.Fragment>
			<div className="form-wrapper">
				<div className="logo">
					<img
						src={urlFormat("/assets/images/logo.svg")}
						alt="logo"
					/>
				</div>

				<h5>Verify your account</h5>

				<form onSubmit={handleSubmit(onSubmit)}>
					<FormGroup>
						<Input
							type="text"
							name="verifyCode"
							placeholder="Verify code"
							innerRef={register({
								required: "This field is required",
							})}
							invalid={errors.verifyCode ? true : false}
							disabled={isProcessing}
						/>
						<FormFeedback>
							{errors.verifyCode && errors.verifyCode.message}
						</FormFeedback>
					</FormGroup>

					<button
						type="submit"
						className="btn btn-primary btn-block"
						disabled={isProcessing}
					>
						{isProcessing ? (
							<i className="fa fa-spinner fa-spin"></i>
						) : (
							"Confirm"
						)}
					</button>
					<hr />
					<span
						className="btn btn-outline-light btn-sm"
						onClick={() => props.actions.toggleForm("verifyForm")}
					>
						Back
					</span>
				</form>
			</div>
		</React.Fragment>
	);
}
