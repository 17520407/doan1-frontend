import React from "react";
import { FormGroup, Input, FormFeedback } from "reactstrap";
import { useForm } from "react-hook-form";
import { urlFormat } from "services/urlFormat";

export default function App(props) {
	const { register, errors, handleSubmit } = useForm();
	const { isProcessing } = props;
	const onSubmit = (data, e) => {
		props.actions.toggleLoading(true);
		props.actions.login(data);
	};

	return (
		<React.Fragment>
			<div className="form-wrapper">
				<div className="logo">
					<img
						src={urlFormat("/assets/images/logo.svg")}
						alt="logo"
					/>
				</div>

				<h5>Sign in</h5>

				<form onSubmit={handleSubmit(onSubmit)}>
					<FormGroup>
						<Input
							type="email"
							name="userEmail"
							placeholder="Username"
							innerRef={register({
								required: "This field is required",
								maxLength: {
									value: 50,
									message:
										"Please enter no more than 50 characters",
								},
							})}
							autoFocus
							invalid={errors.userEmail ? true : false}
							disabled={isProcessing}
						/>
						<FormFeedback>
							{errors.userEmail && errors.userEmail.message}
						</FormFeedback>
					</FormGroup>
					<FormGroup>
						<Input
							type="password"
							name="userPassword"
							placeholder="Password"
							innerRef={register({
								required: "This field is required",
								maxLength: {
									value: 50,
									message:
										"Please enter no more than 50 characters",
								},
							})}
							invalid={errors.userPassword ? true : false}
							disabled={isProcessing}
						/>
						<FormFeedback>
							{errors.userPassword && errors.userPassword.message}
						</FormFeedback>
					</FormGroup>
					<div className="form-group d-flex justify-content-between">
						<span
							className="btn btn-outline-light btn-sm"
							onClick={() =>
								props.actions.toggleForm("verifyForm")
							}
						>
							Verify your account!
						</span>
						<span
							className="btn btn-outline-light btn-sm"
							onClick={() =>
								props.actions.toggleForm("forgotForm")
							}
							disabled={isProcessing}
						>
							Reset password
						</span>
					</div>
					<button type="submit" className="btn btn-primary btn-block">
						{isProcessing ? (
							<i className="fa fa-spinner fa-spin"></i>
						) : (
							"Sign In!"
						)}
					</button>
					<hr />
					<p className="text-muted">Don't have an account?</p>
					<div>
						<span
							className="btn btn-outline-light btn-sm"
							onClick={() => props.actions.toggleForm("authForm")}
						>
							Register now!
						</span>
						&nbsp;
					</div>
				</form>
			</div>
		</React.Fragment>
	);
}
