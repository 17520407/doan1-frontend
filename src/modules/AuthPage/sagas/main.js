import { call, put } from "redux-saga/effects";
import { push } from "react-router-redux";
import { delay } from "redux-saga";
import * as actions from "../actions";
import * as authAPI from "api/auth";
import { NotificationManager } from "react-notifications";
import { takeAction } from "services/forkActionSagas";
import { save } from "services/localStoredService";
import * as wrapperActions from "../../Wrapper/actions";

// Handle Login
export function* handleLogin(action) {
	try {
		let res = yield call(authAPI.login, action.payload);
		switch (res.status) {
			case 200:
				const { accessToken, refreshToken } = res.data.data;
				save("accessToken", accessToken);
				save("refreshToken", refreshToken);
				yield put(
					actions.notify({
						message: res.data.message,
						level: "success",
					})
				);
				yield delay(500);
				yield put(actions.toggleLoading(false));
				yield put(wrapperActions.getUserInfo());
				yield put(actions.loginSuccess(res.data.data));
				yield put(push("/messages"));
				break;
			case 406:
				yield delay(500);
				yield put(
					actions.notify({
						title: "Lỗi",
						message: res.data.message,
						level: "error",
					})
				);
				yield put(actions.toggleLoading(false));
				yield put(actions.loginFail(res.data));
				break;
			default:
				break;
		}
	} catch (err) {
		yield put(
			actions.notify({
				title: "Lỗi",
				message: "Vui lòng kiểm tra lại dữ liệu hoặc đường truyền",
				level: "error",
			})
		);
		yield put(actions.toggleLoading(false));
		yield put(actions.loginFail(err));
	}
}

// Handle Register
export function* handleRegister(action) {
	try {
		let res = yield call(authAPI.register, action.payload);
		switch (res.status) {
			case 200:
				yield put(
					actions.notify({
						message:
							"Tạo tài khoản thành công, vui lòng kiểm tra email để xác thực tài khoản",
						level: "success",
					})
				);
				yield delay(500);
				yield put(actions.toggleLoading(false));
				yield put(actions.registerSuccess(res.data.data));
				break;
			case 406:
				yield delay(500);
				yield put(
					actions.notify({
						title: "Lỗi",
						message: res.data.message,
						level: "error",
					})
				);
				yield put(actions.toggleLoading(false));
				yield put(actions.registerFail(res.data.data));
				break;
			default:
				break;
		}
	} catch (err) {
		yield put(
			actions.notify({
				title: "Lỗi",
				message: "Vui lòng kiểm tra lại dữ liệu hoặc đường truyền",
				level: "error",
			})
		);
		yield put(actions.toggleLoading(false));
		yield put(actions.registerFail(err));
	}
}

// Handle Forgot Password
export function* handleForgotPassword(action) {
	try {
		let res = yield call(authAPI.getVerifyCode, action.payload);
		switch (res.status) {
			case 200:
				yield delay(500);
				NotificationManager.success(
					"Mã xác thực đã được gửi tới email của bạn",
					"Thông báo",
					2000
				);
				yield put(actions.toggleLoading(false));
				yield put(actions.forgotPasswordSuccess(res.data.data));
				break;
			default:
				break;
		}
	} catch (err) {
		NotificationManager.error(
			"Vui lòng kiểm tra lại dữ liệu hoặc đường truyền",
			"Lỗi",
			2000
		);
		yield put(actions.toggleLoading(false));
		yield put(actions.forgotPasswordFail(err));
	}
}

// Handle Update Password
export function* handleUpdatePassword(action) {
	try {
		let res = yield call(authAPI.verifyForgotPassword, action.payload);
		switch (res.status) {
			case 200:
				yield delay(500);
				NotificationManager.success(
					"Mật khẩu đã được cập nhật thành công",
					"Thông báo",
					2000
				);
				yield put(actions.toggleLoading(false));
				yield put(actions.updatePasswordSuccess(res.data.data));
				break;
			default:
				break;
		}
	} catch (err) {
		NotificationManager.error(
			"Vui lòng kiểm tra lại dữ liệu hoặc đường truyền",
			"Lỗi",
			2000
		);
		yield put(actions.toggleLoading(false));
		yield put(actions.updatePasswordFail(err));
	}
}

export function* handleVerifyRegister(action) {
	try {
		let res = yield call(authAPI.verifyRegister, action.payload);
		yield delay(500);
		switch (res.status) {
			case 200:
				yield put(
					actions.notify({
						message: res.data.message,
						level: "success",
					})
				);
				yield put(actions.toggleLoading(false));
				yield put(actions.verifyRegisterSuccess(res.data.data));
				break;
			case 406:
				yield put(
					actions.notify({
						title: "Lỗi",
						message: res.data.message,
						level: "error",
					})
				);
				yield put(actions.toggleLoading(false));
				yield put(actions.verifyRegisterFail());
				break;
			default:
				break;
		}
	} catch (err) {
		yield put(
			actions.notify({
				title: "Lỗi",
				message: "Vui lòng kiểm tra lại dữ liệu hoặc đường truyền",
				level: "error",
			})
		);
		yield put(actions.toggleLoading(false));
		yield put(actions.verifyRegisterFail(err));
	}
}

export function* handleVerifyForgotPassword(action) {
	try {
		let res = yield call(authAPI.verifyForgotPassword, action.payload);
		switch (res.status) {
			case 200:
				yield delay(500);
				yield put(
					actions.notify({
						message: res.data.message,
						level: "success",
					})
				);
				yield put(actions.toggleLoading(false));
				yield put(actions.updatePasswordSuccess(res.data.data));
				break;
			default:
				break;
		}
	} catch (err) {
		yield put(
			actions.notify({
				title: "Lỗi",
				message: "Vui lòng kiểm tra lại dữ liệu hoặc đường truyền",
				level: "error",
			})
		);
		yield put(actions.toggleLoading(false));
		yield put(actions.updatePasswordFail(err));
	}
}

// export function* handleGetUserInfo(action) {
// 	try {
// 		let res = yield call(authAPI.getUserInfo);
// 		if (res.status === 200) {
// 			yield put(actions.getUserInfoSuccess(res.data.data));
// 		} else {
// 			yield put(actions.getUserInfoFail());
// 		}
// 	} catch (err) {
// 		yield put(actions.getUserInfoFail(err));
// 	}
// }

// // Handle Update User Info
// export function* handleUpdateUserInfo(action) {
// 	try {
// 		let res = yield call(authAPI.updateUserInfo, action.payload);
// 		switch (res.status) {
// 			case 200:
// 				const { accessToken, refreshToken } = res.data.data;
// 				save("accessToken", accessToken);
// 				save("refreshToken", refreshToken);
// 				NotificationManager.success(
// 					"Cập nhật thông tin thành công",
// 					"Thành công",
// 					2000
// 				);
// 				yield put(actions.toggleModal("modalUpdateUser"));
// 				yield delay(500);
// 				yield put(actions.updateUserInfoSuccess());
// 				yield put(quizzActions.getUserInfo());
// 				yield put(push("/quizz"));
// 				break;
// 			case 406:
// 				NotificationManager.error("Mã sinh viên đã trùng", "Lỗi", 2000);
// 				yield put(actions.updateUserInfoFail());
// 				break;
// 			default:
// 				break;
// 		}
// 	} catch (err) {
// 		yield put(actions.updateUserInfoFail(err));
// 	}
// }

/*----------------------------------------------------------*/
export function* login() {
	yield takeAction(actions.login, handleLogin);
}
export function* register() {
	yield takeAction(actions.register, handleRegister);
}
export function* verifyRegister() {
	yield takeAction(actions.verifyRegister, handleVerifyRegister);
}
export function* forgotPassword() {
	yield takeAction(actions.forgotPassword, handleForgotPassword);
}
export function* updatePassword() {
	yield takeAction(actions.updatePassword, handleUpdatePassword);
}
/*----------------------------------------------------------*/
export default [
	login,
	register,
	verifyRegister,
	forgotPassword,
	updatePassword,
];
