/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";
import WEB_RTC_CODE from "../../constants/webRtcCode";

export const name = "MessagesPage";

const initialState = freeze({
	isContactOpened: false,
	isMusicOpened: false,
	toggleVideoCall: false,
	activeTab: 1,
	isGetRoomSuccess: false,
	isUploadSuccess: false,
	isSeenMessage: false,
	isUploading: false,
	isLoadingMessages: true,
	isLoading: true,

	webRtcErrorMessages: null,

	answerWebRtcId: null,
	answerSocketId: null,
	isOnCall: false,
	isCaller: false,

	typingUserInfo: {
		userId: "",
		isTyping: false,
	},

	urlImage: "",
	userId: "",
	userInfo: {},

	curRoom: {},
	curSendingId: null,
	curSong: null,

	listMessages: [],
	listNotifications: [],
	listFiles: [],
	listImages: [],
	listRooms: [],
	listSongs: [],
});
export default handleActions(
	{
		/*---
        ----- Toggle Modal
        */
		[actions.toggleModal]: (state, action) => {
			return freeze({
				...state,
				toggleVideoCall: !state.toggleVideoCall,
			});
		},
		/*---
        ----- Toggle Side Bar
        */
		[actions.toggleSideBar]: (state, action) => {
			const { type } = action.payload;
			if (type === "contactInfo") {
				if (!state.isContactOpened) {
					let listMessages = [...state.listMessages];
					let listImages = listMessages.filter(
						(item) => item.type === "image"
					);
					let listFiles = listMessages.filter(
						(item) => item.type === "file"
					);
					return freeze({
						...state,
						isContactOpened: !state.isContactOpened,
						isMusicOpened: false,
						listFiles,
						listImages,
					});
				}
				return freeze({
					...state,
					isContactOpened: !state.isContactOpened,
					isMusicOpened: false,
				});
			}
			if (type === "musicList") {
				if (!state.isMusicOpened) {
					return freeze({
						...state,
						isContactOpened: false,
						isMusicOpened: !state.isMusicOpened,
					});
				}
				return freeze({
					...state,
					isContactOpened: false,
					isMusicOpened: !state.isMusicOpened,
				});
			}
			return freeze({
				...state,
			});
		},
		/*---
        ----- Handle Cur Song
        */
		[actions.handleCurSong]: (state, action) => {
			let curSong = action.payload;
			if (typeof curSong === "string") {
				curSong = state.listSongs.find(
					(song) => song.url === action.payload
				);
			}
			if (curSong === null) {
				curSong = {};
			}
			return freeze({
				...state,
				curSong,
			});
		},
		/*---
        ----- Handle Connect WebRtc Success
        */
		[actions.handleConnectWebRtcSuccess]: (state, action) => {
			const { code, response } = action.payload;
			switch (code) {
				case WEB_RTC_CODE.HAVING_A_CALL:
					return freeze({
						...state,
						toggleVideoCall: true,
						answerWebRtcId: response.callerId,
						answerSocketId: response.callerSocketId,
					});
				default:
					return freeze({
						...state,
					});
			}
		},
		/*---
        ----- Handle Connect WebRtc Fail
        */
		[actions.handleConnectWebRtcFail]: (state, action) => {
			return freeze({
				...state,
				webRtcErrorMessages: action.payload,
			});
		},
		/*---
        ----- Handle Receiver Accept Call
        */
		[actions.handleReceiverAcceptCall]: (state, action) => {
			return freeze({
				...state,
				isOnCall: true,
				toggleVideoCall: false,
			});
		},
		/*---
        ----- Handle Caller Make Call
        */
		[actions.handleCallerMakeCall]: (state, action) => {
			return freeze({
				...state,
				isCaller: true,
				toggleVideoCall: true,
			});
		},
		/*---
        ----- Handle Caller Cancel Call
        */
		[actions.handleCancelCall]: (state, action) => {
			return freeze({
				...state,
				isCaller: false,
				isOnCall: false,
				answerWebRtcId: null,
				answerSocketId: null,
				webRtcErrorMessages: null,
				toggleVideoCall: false,
			});
		},
		/*---
        ----- Handle Typing
        */
		[actions.handleClear]: (state, action) => {
			return freeze({
				...initialState,
				userId: state.userId,
			});
		},
		/*---
        ----- Handle Typing
        */
		[actions.handleTyping]: (state, action) => {
			return freeze({
				...state,
				typingUserInfo: action.payload,
			});
		},
		/*---
        ----- Handle Cur Tab
        */
		[actions.handleCurTab]: (state, action) => {
			return freeze({
				...state,
				activeTab: action.payload,
			});
		},
		/*---
        ----- Handle Cur Room
        */
		[actions.handleCurRoom]: (state, action) => {
			return freeze({
				...state,
				curRoom: action.payload,
			});
		},
		/*---
        ----- Upload Image
        */
		[actions.uploadImage]: (state, action) => {
			return freeze({
				...state,
				isUploading: true,
				isUploadSuccess: false,
			});
		},
		[actions.uploadImageSuccess]: (state, action) => {
			return freeze({
				...state,
				isUploading: false,
				isUploadSuccess: true,
				urlImage: action.payload.url,
			});
		},
		[actions.uploadImageFail]: (state, action) => {
			return freeze({
				...state,
				isUploading: false,
				isUploadSuccess: false,
			});
		},
		/*---
        ----- Upload File
        */
		[actions.uploadFile]: (state, action) => {
			return freeze({
				...state,
				isUploading: true,
				isUploadSuccess: false,
			});
		},
		[actions.uploadFileSuccess]: (state, action) => {
			return freeze({
				...state,
				isUploading: false,
				isUploadSuccess: true,
				urlImage: action.payload.url,
			});
		},
		[actions.uploadFileFail]: (state, action) => {
			return freeze({
				...state,
				isUploading: false,
				isUploadSuccess: false,
			});
		},
		/*---
        ----- Get User Info
        */
		[actions.getUserInfo]: (state, action) => {
			return freeze({
				...state,
				userId: action.payload._id,
				userInfo: action.payload,
			});
		},
		/*---
        ----- Get Connect Room
        */
		[actions.getConnectRoom]: (state, action) => {
			return freeze({
				...state,
				curSendingId: action.payload.sendingId,
				curRoom: {
					connectId: action.payload.sendingId,
					connectName: action.payload.userName,
					connectAvatar: action.payload.userAvatar,
				},
			});
		},
		/*---
        ----- Get All Songs
        */
		[actions.getAllSongsSuccess]: (state, action) => {
			if (action.payload.payload.length > 0) {
				let listSongs = action.payload.payload;
				return freeze({
					...state,
					listSongs,
				});
			}
			return freeze({
				...state,
			});
		},
		/*---
        ----- Get All Room
        */
		[actions.getAllRooms]: (state, action) => {
			return freeze({
				...state,
				isGetRoomSuccess: false,
			});
		},
		[actions.getAllRoomsSuccess]: (state, action) => {
			let data = action.payload;
			if (data.length > 0) {
				data.reverse();
				let curSendingId = state.curSendingId;
				let curRoom = state.curRoom;
				if (curSendingId !== null) {
					const index = data.findIndex(
						(room) => room.connectId === curSendingId
					);
					if (index !== -1) {
						curRoom = data[index];
					}
				} else {
					curRoom = data[0];
				}
				return freeze({
					...state,
					listRooms: data,
					curRoom,
					isLoading: false,
					isGetRoomSuccess: true,
				});
			} else {
				return freeze({
					...state,
					isLoading: false,
					isGetRoomSuccess: true,
				});
			}
		},
		[actions.getAllRoomsFail]: (state, action) => {
			return freeze({
				...state,
				isGetRoomSuccess: true,
				isLoading: false,
			});
		},
		/*---
        ----- Get All Message
        */
		[actions.getAllMessages]: (state, action) => {
			let curRoom = state.curRoom;
			if (!curRoom.hasOwnProperty("roomId")) {
				// console.log(action.payload.roomId);
				return freeze({
					...state,
					curRoom: {
						...curRoom,
						roomId: action.payload.roomId,
					},
					isLoadingMessages: true,
					isGetRoomSuccess: false,
				});
			}
			return freeze({
				...state,
				isLoadingMessages: true,
				isGetRoomSuccess: false,
			});
		},
		[actions.getAllMessagesSuccess]: (state, action) => {
			return freeze({
				...state,
				isLoadingMessages: false,
				listMessages: action.payload.messages,
			});
		},
		[actions.getAllMessagesFail]: (state, action) => {
			return freeze({
				...state,
				listMessages: [],
				isLoadingMessages: false,
			});
		},
		/*---
        ----- Get All Notifications
        */
		[actions.getAllNotificationsSuccess]: (state, action) => {
			return freeze({
				...state,
				listNotifications: action.payload,
			});
		},
		/*---
        ----- Update Messages
        */
		[actions.updateMessages]: (state, action) => {
			let listMessages = [...state.listMessages];
			let typingUserInfo = {
				isTyping: false,
			};
			listMessages.push(action.payload);
			if (action.payload.type === "image") {
				return freeze({
					...state,
					isUploadSuccess: false,
					listMessages,
					typingUserInfo,
				});
			} else {
				return freeze({
					...state,
					listMessages,
					typingUserInfo,
				});
			}
		},
		/*---
        ----- Update Rooms
        */
		[actions.updateRooms]: (state, action) => {
			let listRooms = [...state.listRooms];
			let curRoom = state.curRoom;
			let roomMessage = action.payload;
			const index = listRooms.findIndex(
				(room) => room.roomId === roomMessage.roomId
			);
			if (index !== -1) {
				listRooms[index] = {
					...listRooms[index],
					lastMessage: {
						...listRooms[index].lastMessage,
						message: roomMessage.lastMessage.message,
						type: roomMessage.lastMessage.type,
						time: roomMessage.lastMessage.time,
					},
				};
				curRoom = {
					...curRoom,
					lastMessage: {
						...curRoom.lastMessage,
						message: roomMessage.lastMessage.message,
						type: roomMessage.lastMessage.type,
						time: roomMessage.lastMessage.time,
					},
				};
			} else {
				let newRoom = {
					...state.curRoom,
					...roomMessage,
				};
				listRooms.push(newRoom);
			}
			return freeze({
				...state,
				listRooms,
				curRoom,
			});
		},
	},
	initialState
);
