import React, { Component } from "react";
import { Modal, ModalBody } from "reactstrap";
import { ReactSVG } from "react-svg";
import { socket } from "services/api";
import config from "../../../../config";
import globalAudio from "services/globalAudio";
import FVideo from "../svg/f-video.svg";
import FX from "../svg/f-x.svg";
import MESSAGES from "../../../../constants/messages";

const renderButton = (props) => {
	const { curRoom, userId, isCaller, answerWebRtcId, answerSocketId } = props;

	const denyCall = () => {
		socket.emit(MESSAGES.CLIENT_SEND_WEB_RTC_NOT_RECEIVE_CALL, {
			userId,
			answerId: answerWebRtcId,
			answerSocketId,
		});
		globalAudio.pause("audioCall");
		props.actions.handleCancelCall();
	};

	const cancelCall = () => {
		socket.emit(MESSAGES.CLIENT_SEND_WEB_RTC_NOT_WANT_CALL, {
			userId,
			answerId: curRoom.connectId,
		});
		globalAudio.pause("audioCall");
		props.actions.handleCancelCall();
	};
	const acceptCall = () => {
		socket.emit(MESSAGES.CLIENT_WEB_RTC_SEND_ANSWER, {
			userId,
			answerId: answerWebRtcId,
			answerSocketId,
		});
		globalAudio.pause("audioCall");
		props.actions.handleReceiverAcceptCall({
			answerId: answerWebRtcId,
			answerSocketId,
		});
		window.open(
			`${config.apiURL}/video?isCaller=false&userId=${userId}&answerId=${answerWebRtcId}&&answerSocketId=${answerSocketId}&socketId=${socket.id}`,
			"Video call",
			"width=1000, height=600"
		);
	};

	return (
		<React.Fragment>
			{isCaller ? (
				<button
					type="button"
					className="btn btn-danger btn-floating btn-lg"
					onClick={cancelCall}
				>
					<ReactSVG src={FX} />
				</button>
			) : (
				<React.Fragment>
					<button
						type="button"
						className="btn btn-danger btn-floating btn-lg"
						onClick={denyCall}
					>
						<ReactSVG src={FX} />
					</button>
					<button
						type="button"
						className="btn btn-success btn-pulse btn-floating btn-lg"
						onClick={acceptCall}
					>
						<ReactSVG src={FVideo} />
					</button>
				</React.Fragment>
			)}
		</React.Fragment>
	);
};

const renderAction = (props) => {
	const { curRoom, isCaller } = props;
	return (
		<React.Fragment>
			<div>
				{isCaller ? "You" : curRoom.connectName}
				<span className="text-success"> video calling to </span>
				{isCaller ? curRoom.connectName : "you"}
			</div>
			<div className="action-button">{renderButton(props)}</div>
		</React.Fragment>
	);
};

const renderError = (actions, curRoom, webRtcErrorMessages) => (
	<React.Fragment>
		<h4>
			{curRoom.connectName}{" "}
			<span className="text-warning">{webRtcErrorMessages.message}</span>
		</h4>
		<div className="action-button">
			<button
				type="button"
				className="btn btn-danger btn-floating btn-lg"
				onClick={() => actions.toggleModal()}
			>
				<ReactSVG src={FX} />
			</button>
		</div>
	</React.Fragment>
);

class VideoCall extends Component {
	render() {
		const {
			isCaller,
			curRoom,
			toggleVideoCall,
			webRtcErrorMessages,
			userInfo,
		} = this.props;

		return (
			<Modal
				isOpen={toggleVideoCall}
				centered
				// external={externalCloseBtn}
			>
				<ModalBody>
					<div className="call">
						<figure className="mb-4 avatar avatar-xl">
							<img
								src={`https://${
									isCaller
										? curRoom.connectAvatar
										: userInfo.userAvatar
								}`}
								// src={urlFormat(
								// 	"/assets/images/women_avatar1.jpg"
								// )}
								className="rounded-circle"
								alt="video__avt"
							/>
						</figure>
						{webRtcErrorMessages
							? renderError(
									this.props.actions,
									curRoom,
									webRtcErrorMessages
							  )
							: renderAction(this.props)}
					</div>
					{/* )} */}
				</ModalBody>
			</Modal>
		);
	}
}

export default VideoCall;
