import React, { Component } from "react";
import ChatMessageItem from "./ChatMessageItem";
import _ from "lodash";
import * as styles from "./chat.module.css";
import { urlFormat } from "services/urlFormat";
import classnames from "classnames";

const renderLoadingImage = () => (
	<div className="message-item outgoing-message">
		<figure className="message-image">
			<div className={styles.message_image_preview}>
				<div className="spinner-border text-primary" role="status">
					<span className="sr-only">Loading...</span>
				</div>
			</div>
		</figure>
	</div>
);

const renderTyping = (typingUserInfo, userId, curRoom, userInfo) => (
	<div
		className={classnames("message-item", {
			"outgoing-message": typingUserInfo.userId === userId,
		})}
	>
		<div className="message-avatar">
			<figure className="avatar">
				<img
					// src={urlFormat("/assets/images/women_avatar5.jpg")}
					src={
						typingUserInfo.userId === userId
							? `https://${userInfo.userAvatar}`
							: `https://${curRoom.connectAvatar}`
					}
					className="rounded-circle"
					alt="avt"
				/>
			</figure>
			<div className="bubble-chat">
				<div className="container-circle">
					<div className="circle cc1" id="circle1"></div>
					<div className="circle cc2" id="circle2"></div>
					<div className="circle cc3" id="circle3"></div>
				</div>
			</div>
		</div>
	</div>
);

class ChatMessage extends Component {
	scrollToBottom = () => {
		this.messagesEnd.scrollIntoView({ behavior: "smooth" });
	};

	componentDidMount() {
		this.scrollToBottom();
	}

	componentDidUpdate() {
		this.scrollToBottom();
	}

	render() {
		const {
			listMessages,
			userId,
			userInfo,
			curRoom,
			typingUserInfo,
			isUploading,
		} = this.props;
		return (
			<div
				className={`chat-body ${styles.scroll_bar__thin}`}
				tabIndex={1}
			>
				<div className="messages">
					{_.isEmpty(listMessages) ? (
						<div className="no-message-container">
							<div className="row mb-5">
								<div className="col-md-4 offset-4">
									<img
										src={urlFormat(
											"/assets/images/undraw_empty_xct9.svg"
										)}
										className="img-fluid"
										alt="empty_messages"
									/>
								</div>
							</div>
							<p className="lead text-center">
								Type something to your friend
							</p>
						</div>
					) : (
						listMessages.map((item, index) => {
							return (
								<ChatMessageItem
									item={item}
									userInfo={userInfo}
									userId={userId}
									curRoom={curRoom}
									key={`message__${index}`}
								/>
							);
						})
					)}
					{isUploading && renderLoadingImage()}
					{typingUserInfo.isTyping &&
						renderTyping(typingUserInfo, userId, curRoom, userInfo)}
					<div
						style={{ float: "left", clear: "both" }}
						ref={(el) => {
							this.messagesEnd = el;
						}}
					/>
				</div>
			</div>
		);
	}
}

export default ChatMessage;
