import React from "react";
import Avatar from "./ChatSideAvatar";
import styles from "./sidebar.module.css";
import moment from "moment";
import classnames from "classnames";
import { socket } from "services/api";
import MESSAGES from "../../../../constants/messages";

const renderLastMessageWithType = (lastMessage) => {
	let node = React.node;
	switch (lastMessage.type) {
		case "image":
			node = (
				<p>
					<i className="fa fa-camera mr-1"></i> Hình ảnh
				</p>
			);
			break;
		case "file":
			node = (
				<p>
					<i className="fa fa-file"></i> File
				</p>
			);
			break;
		default:
			node = <p>{lastMessage.message}</p>;
			break;
	}
	return node;
};

const chatSideItem = ({ roomInfo, curRoom, actions }) => {
	let lastMessage = roomInfo.lastMessage;
	if (!roomInfo.hasOwnProperty("lastMessage")) {
		lastMessage = curRoom.lastMessage;
	}

	const handleChangeRoom = () => {
		if (roomInfo.roomId !== curRoom.roomId) {
			socket.emit(MESSAGES.CLIENT_LEAVE_ROOM, {
				roomId: curRoom.roomId,
            });
            actions.postSeenMessage({
                roomId: roomInfo.roomId,
            });
			actions.handleCurRoom(roomInfo);
			actions.getAllMessages({ roomId: roomInfo.roomId });
		}
	};

	return (
		<li
			className={classnames("list-group-item", {
				"open-chat": curRoom.roomId === roomInfo.roomId,
			})}
		>
			<Avatar curRoom={curRoom} roomInfo={roomInfo} />
			<div className="users-list-body">
				<div onClick={handleChangeRoom}>
					<h5>
						{roomInfo.connectName
							? roomInfo.connectName
							: "Chưa có"}
					</h5>
					{renderLastMessageWithType(lastMessage)}
				</div>
				<div className={styles.usersListAction}>
					<small className="text-muted">
						{moment(lastMessage.time).format("HH:mm A")}
					</small>
				</div>
			</div>
		</li>
	);
};
export default chatSideItem;
