import React from "react";
import { ReactSVG } from "react-svg";
// import FUser from "../svg/f-user.svg";
// import FPlusCircle from "../svg/f-plus-circle.svg";
import FX from "../svg/f-x.svg";
import styles from "./sidebar.module.css";
import BellNotifications from "./BellNotifications";

const chatSideHeader = (props) => {
	return (
		<React.Fragment>
			<header>
				<span>Chats</span>
				<ul className="list-inline">
					<li className="list-inline-item">
						<span className={`btn btn-outline-light ${styles.bellNotificationIcon}`}>
							<BellNotifications listNotifications={props.listNotifications} actions={props.actions} />
						</span>
					</li>
					{/* <li className="list-inline-item">
						<span
							className="btn btn-outline-light"
							data-toggle="tooltip"
							title=""
							data-navigation-target="friends"
							data-original-title="New chat"
						>
							<ReactSVG src={FPlusCircle} />
						</span>
					</li> */}
					<li className="list-inline-item d-xl-none d-inline">
						<span className="btn btn-outline-light text-danger sidebar-close">
							<ReactSVG src={FX} />
						</span>
					</li>
				</ul>
			</header>
			<form>
				<input
					type="text"
					className="form-control"
					placeholder="Search chats"
					autoFocus
				/>
			</form>
		</React.Fragment>
	);
};

export default chatSideHeader;
