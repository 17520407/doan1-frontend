import React from "react";
// import { urlFormat } from "services/urlFormat";
import classnames from "classnames";

const avatar = ({ curRoom, roomInfo }) => (
	<figure
		className={classnames("avatar", {
			"avatar-state-success": roomInfo.seen === false,
		})}
	>
		<img
			src={`https://${roomInfo.connectAvatar}`}
			// src={urlFormat("/assets/images/man_avatar3.jpg")}
			className="rounded-circle"
			alt="avt"
		/>
	</figure>
);
export default avatar;
