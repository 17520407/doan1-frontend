import React, { Component } from "react";
import ChatSideBar from "./SideBar/ChatSideBar.js";
import ChatMain from "./Chat/ChatMain";
import MESSAGES from "../../../constants/messages";
import LoadingScreen from "../../common/LoadingScreen";
import ContactInformation from "./ContactInfomation/ContactInformation";
import MusicList from "./Music/MusicList";
import ModalVideoCall from "./Call/VideoCall";
import { urlFormat } from "services/urlFormat";
import { socket } from "services/api";
import _ from "lodash";
import 'emoji-mart/css/emoji-mart.css'

const renderEmptyRoom = () => (
	<div
		style={{
			width: "100%",
			height: "100%",
			display: "flex",
			justifyContent: "center",
			alignItems: "center",
		}}
	>
		<div className="no-message-container">
			<div className="row mb-5">
				<div className="col-md-4 offset-4">
					<img
						src={urlFormat("/assets/images/undraw_empty_xct9.svg")}
						className="img-fluid"
						alt="empty_messages"
					/>
				</div>
			</div>
			<p className="lead text-center">You don't have any room chat</p>
		</div>
	</div>
);

class MessagesPage extends Component {
	componentDidMount() {
		const { userId, curRoom } = this.props;
		socket.on(MESSAGES.DISCONNECT, () => {
			socket.connect();
			socket.emit(MESSAGES.CLIENT_JOIN_ROOM, {
				sendingId: curRoom.roomId,
				userId,
			});
			socket.emit(MESSAGES.CLIENT_SEND_SOCKET, {
				userId,
			});
		});
		this.props.actions.getAllRooms();
		this.props.actions.getAllNotifications();
		this.props.actions.getAllSongs();
	}
	componentDidUpdate(prevProps, prevState) {
		const { isGetRoomSuccess, curRoom, userId, curSendingId } = this.props;
		if (
			isGetRoomSuccess !== prevProps.isGetRoomSuccess &&
			isGetRoomSuccess &&
			!_.isEmpty(curRoom) &&
			curSendingId === null
		) {
			socket.emit(MESSAGES.CLIENT_JOIN_ROOM, {
				userId,
				sendingId: curRoom.connectId,
			});
		}
	}

	componentWillUnmount() {
		const { curRoom } = this.props;
		socket.emit(MESSAGES.CLIENT_LEAVE_ROOM, {
			roomId: curRoom.roomId,
		});
		this.props.actions.handleClear();
	}
	render() {
		const {
			isLoading,
			listRooms,
			curSendingId,
		} = this.props;
		return (
			<React.Fragment>
				{isLoading ? (
					<div style={{ width: "100%", height: "100%" }}>
						<LoadingScreen />
					</div>
				) : listRooms.length > 0 || curSendingId !== null ? (
					<div className="content">
						<ChatSideBar {...this.props} />
						<ChatMain {...this.props} />
						<ContactInformation {...this.props} />
						<MusicList {...this.props} />
					</div>
				) : (
					renderEmptyRoom()
				)}
				<ModalVideoCall {...this.props} />
			</React.Fragment>
		);
	}
}

export default MessagesPage;
