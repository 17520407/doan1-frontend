import React, { useEffect } from "react";
import FriendISearchItem from "./FriendISearchItem";
import { ReactSVG } from "react-svg";
import FUserPlus from "../../../common/svg/f-user-plus.svg";
import FX from "../../../common/svg/f-x.svg";

// import classnames from "classnames";
// import styles from "./sidebar.module.css";
import _ from "lodash";
import { useInput } from "../InputHook";

export default function FriendSideBar(props) {
	const { searchList, message } = props;
	const { value, bind } = useInput("");
	useEffect(() => {
		props.actions.searchUser({
			userName: "",
			page: 0,
		});
	}, [props.actions]);

	const handleSearch = (evt) => {
		props.actions.searchUser({
			userName: value.trim(),
			page: 0,
		});
		evt.preventDefault();
	};

	return (
		<React.Fragment>
			<div className="sidebar-group">
				<div id="friends" className="sidebar active">
					<header>
						<span>Friends</span>
						<ul className="list-inline">
							<li
								className="list-inline-item"
								data-toggle="tooltip"
								title=""
								data-original-title="Add friends"
							>
								<span
									className="btn btn-outline-light"
									data-toggle="modal"
									data-target="#addFriends"
								>
									<ReactSVG src={FUserPlus} />
								</span>
							</li>
							<li className="list-inline-item d-xl-none d-inline">
								<span className="btn btn-outline-light text-danger sidebar-close">
									<ReactSVG src={FX} />
								</span>
							</li>
						</ul>
					</header>
					<form onSubmit={handleSearch}>
						<input
							// onFocusCapture={() =>
							// 	props.actions.searchUser({
							// 		userName: "",
							// 		page: 0,
							// 	})
							// }
							type="text"
							className="form-control"
							placeholder="Search friends"
							autoFocus
							{...bind}
							onKeyPress={(e) => {
								if (e.key === "Enter") {
									handleSearch(e);
								}
							}}
						/>
					</form>
					<div
						className="sidebar-body"
						style={{
							overflow: "hidden",
							outline: "currentcolor none medium",
						}}
					>
						<ul className="list-group list-group-flush">
							{_.isEmpty(searchList) ? (
								<li className="list-group-item">
									<p>{message}</p>
								</li>
							) : (
								searchList.map((item, index) => {
									return (
										<FriendISearchItem
											key={`friend_item__${index}`}
											props={props}
											item={item}
										/>
									);
								})
							)}
						</ul>
					</div>
				</div>
			</div>
		</React.Fragment>
	);
}
