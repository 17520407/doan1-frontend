import React from "react";
import styles from "./friendsList.module.css";
import FriendItemAction from "./FriendItemAction";

const FriendListItem = ({ item, props }) => {
	return (
		<li className={styles.friendGroupItem}>
			<div>
				<figure className={styles.avatar}>
					<img
						src={`https://${item.userAvatar}`}
						className={styles.roundedSquare}
						alt="avatar"
					/>
				</figure>
			</div>
			<div className={styles.usersListBody}>
				<div className={styles.userInfo}>
					<div className={styles.userInfoName}>{item.userName}</div>
					<small>{item.userGender}</small>
				</div>
				<FriendItemAction props={props} item={item} />
			</div>
		</li>
	);
};

export default FriendListItem;
