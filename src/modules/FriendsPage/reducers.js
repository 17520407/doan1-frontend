/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";

export const name = "FriendsPage";

const initialState = freeze({
	isLoading: false,
	isSearchLoading: false,
	/*--------------- */
	curType: 1,
	curUserId: {},
	friendsList: [],
	searchList: [],
	userInfo: {},
	quantity: {},
	message: "",
});

export default handleActions(
	{
		/*---
        ----- Toggle Loading
        */
		[actions.toggleLoading]: (state, action) => {
			if (action.payload.type === "friendList") {
				return freeze({
					...state,
					isLoading: action.payload.value,
				});
			}
			if (action.payload.type === "friendSearch") {
				return freeze({
					...state,
					isSearchLoading: action.payload.value,
				});
			}
			return freeze({
				...state,
			});
		},
		/*---
        -----  Get User Info
        */
		[actions.getUserInfo]: (state, action) => {
			return freeze({
				...state,
				userInfo: action.payload,
			});
		},
		/*---
        ----- Get All Friends
        */
		[actions.getAllFriendsSuccess]: (state, action) => {
			return freeze({
				...state,
				curType: 1,
				friendsList: action.payload,
			});
		},
		[actions.getAllFriendsFail]: (state, action) => {
			return freeze({
				...state,
				friendsList: [],
				curType: 1,
				message: action.payload,
			});
		},
		/*---
        ----- Get All Request
        */
		[actions.getAllRequestsSuccess]: (state, action) => {
			if (action.payload.length === 0) {
				return freeze({
					...state,
					friendsList: action.payload,
					message: "You don't have anyone following",
					curType: 2,
				});
			}
			return freeze({
				...state,
				friendsList: action.payload,
				curType: 2,
			});
		},
		[actions.getAllRequestsFail]: (state, action) => {
			return freeze({
				...state,
				friendsList: [],
				message: action.payload,
				curType: 2,
			});
		},
		/*---
        ----- Get All Pending
        */
		[actions.getAllPendingSuccess]: (state, action) => {
			if (action.payload.length === 0) {
				return freeze({
					...state,
					friendsList: action.payload,
					message: "You don't have anyone following",
					curType: 3,
				});
			}
			return freeze({
				...state,
				friendsList: action.payload,
				curType: 3,
			});
		},
		[actions.getAllPendingFail]: (state, action) => {
			return freeze({
				...state,
				friendsList: [],
				message: action.payload,
				curType: 3,
			});
		},
		/*---
        ----- Search User
        */
		[actions.searchUserSuccess]: (state, action) => {
			let lastObj = action.payload.pop();
			if (lastObj.quantity < 1) {
				return freeze({
					...state,
					searchList: [],
					message: "This user is not exist",
				});
			}
			return freeze({
				...state,
				searchList: action.payload,
				quantity: lastObj.quantity,
			});
		},
		[actions.searchUserFail]: (state, action) => {
			return freeze({
				...state,
				searchList: [],
				message: action.payload,
			});
		},
		/*---
        -----  Post Friend Request
        */
		[actions.postFriendRequestSuccess]: (state, action) => {
			let searchList = [...state.searchList];
			let friendRequests = action.payload.friendRequests;
			searchList.forEach((itemInList) => {
				const index = friendRequests.findIndex(
					(item) => item.userRequestId === itemInList._id
				);
				searchList[index] = {
					...searchList[index],
					friendStatus: "request",
				};
			});
			return freeze({
				...state,
				searchList,
			});
		},
		/*---
        -----  Post Friend Accept
        */
		[actions.postFriendAccept]: (state, action) => {
			return freeze({
				...state,
				curUserId: action.payload.curUserId,
			});
		},
		[actions.postFriendAcceptSuccess]: (state, action) => {
			let friendsList = [...state.friendsList];
			const index = friendsList.findIndex(
				(item) => item._id === state.curUserId
			);
			friendsList.splice(index, 1);
			return freeze({
				...state,
				friendsList,
			});
		},
		[actions.postFriendAcceptFail]: (state, action) => {
			return freeze({
				...state,
			});
		},
		/*---
        -----  Post Friend Accept
        */
		[actions.postFriendCancelRequest]: (state, action) => {
			return freeze({
				...state,
				curUserId: action.payload,
			});
		},
		[actions.postFriendCancelRequestSuccess]: (state, action) => {
			let friendsList = [...state.friendsList];
			let searchList = [...state.searchList];
			let message = "";
			const index = friendsList.findIndex(
				(item) => item.userRequestId === state.curUserId.userRequestId
			);
			const searchIndex = searchList.findIndex(
				(item) => item._id === state.curUserId.userRequestId
			);
			searchList[searchIndex] = {
				...searchList[searchIndex],
				friendStatus: "normal",
			};
			friendsList.splice(index, 1);
			if (friendsList.length < 1) {
				message = "You don't have anyone following";
			}
			return freeze({
				...state,
				friendsList,
				searchList,
				message,
				curUserId: {},
			});
		},
	},
	initialState
);
