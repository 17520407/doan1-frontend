import React from "react";
import { Modal, ModalHeader } from "reactstrap";
import UpdateUserForm from "./UpdateUserForm";

import { ReactSVG } from "react-svg";
import FEdit from "./svg/f-edit.svg";
const ModalUpdateUser = (props) => {
	const { toggleModalUpdate } = props;
	return (
		<Modal isOpen={toggleModalUpdate} centered>
			<ModalHeader toggle={() => props.actions.toggleModalUpdate()}>
				<ReactSVG src={FEdit} />
				Edit Profile
			</ModalHeader>
			<UpdateUserForm {...props} />
		</Modal>
	);
};

export default ModalUpdateUser;
