import React from "react";
import { Link } from "react-router-dom";
// import { urlFormat } from "services/urlFormat";
import { ReactSVG } from "react-svg";
import FMessLogo from "./svg/f-mess-logo.svg";
import FMessCircle from "./svg/f-mess-circle.svg";
import FUser from "./svg/f-user.svg";

const NavBar = (props) => {
	const { userInfo } = props;
	return (
		<nav className="navigation">
			<div className="nav-group">
				<ul>
					<li className="logo">
						<a href="#">
							{/* style={{
									enableBackground: "new 0 0 612 612",
								}} */}
							<ReactSVG src={FMessLogo} />
						</a>
					</li>
					<li>
						<Link
							to="/messages"
							className={
								props.location.pathname === "/messages"
									? "active"
									: ""
							}
							data-toggle="tooltip"
							title=""
							data-placement="right"
							data-original-title="Chats"
						>
							<span className="badge badge-warning"></span>
							<ReactSVG src={FMessCircle} />
						</Link>
					</li>
					<li className="brackets">
						<Link
							to="/friends"
							className={
								props.location.pathname === "/friends"
									? "active"
									: ""
							}
							data-toggle="tooltip"
							title=""
							data-placement="right"
							data-original-title="Friends"
						>
							<span className="badge badge-danger"></span>
							<ReactSVG src={FUser} />
						</Link>
					</li>
					{/* <li>
						<span
							className="dark-light-switcher"
							data-toggle="tooltip"
							title=""
							data-placement="right"
							data-original-title="Dark mode"
						>
							<svg
								xmlns="http://www.w3.org/2000/svg"
								width="24"
								height="24"
								viewBox="0 0 24 24"
								fill="none"
								stroke="currentColor"
								strokeWidth="2"
								strokeLinecap="round"
								strokeLinejoin="round"
								className="feather feather-moon"
							>
								<path d="M21 12.79A9 9 0 1 1 11.21 3 7 7 0 0 0 21 12.79z"></path>
							</svg>
						</span>
					</li> */}
					<li
						data-toggle="tooltip"
						title=""
						data-placement="right"
						data-original-title="User menu"
					>
						<span data-toggle="dropdown">
							<figure
								className="avatar"
								style={{
									border: "1px solid rgba(0,0,0,0.2)",
								}}
							>
								<img
									src={`https://${userInfo.userAvatar}`}
									className="rounded-circle"
									alt="avt"
								/>
							</figure>
						</span>
						<div className="dropdown-menu">
							<span
								className="dropdown-item"
								onClick={() =>
									props.actions.toggleModalUpdate()
								}
							>
								Edit profile
							</span>
							{/* <span
								className="dropdown-item"
								// data-navigation-target="contact-information"
								onClick={() =>
									props.actions.toggleSideBar({
										type: "contactInfo",
									})
								}
							>
								Profile
							</span> */}
							{/* <span className="dropdown-item">Settings</span> */}
							<div className="dropdown-divider"></div>
							<span
								onClick={() => props.actions.logOut()}
								className="dropdown-item text-danger"
							>
								Logout
							</span>
						</div>
					</li>
				</ul>
			</div>
		</nav>
	);
};
export default NavBar;
