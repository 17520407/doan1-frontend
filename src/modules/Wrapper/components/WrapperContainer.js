import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { name } from "../reducers";
import * as action from "../actions";
import Wrapper from "./Wrapper";
import * as messageActions from "../../MessagesPage/actions";
import * as friendActions from "../../FriendsPage/actions";
import { NotificationContainer } from "react-notifications";

class WrapperContainer extends Component {
	render() {
		return (
			<React.Fragment>
				<NotificationContainer />
				<Wrapper {...this.props} />
			</React.Fragment>
		);
	}
}

function mapStateToProps(state) {
	return {
		...state[name],
	};
}
function mapDispatchToProps(dispatch) {
	const actions = {
		...action,
		...messageActions,
		...friendActions,
	};
	return { actions: bindActionCreators(actions, dispatch) };
}
export default withRouter(
	connect(mapStateToProps, mapDispatchToProps)(WrapperContainer)
);
