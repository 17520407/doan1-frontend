import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { socket } from "services/api";
import NotificationSystem from "react-notification-system";
import MESSAGES from "../../../constants/messages";
import Draggable from "react-draggable";
import NavBar from "./NavBar";
import _ from "lodash";
import { ReactSVG } from "react-svg";
import { urlFormat } from "services/urlFormat";
import globalAudio from "services/globalAudio";
import ModalUpdateUser from "./ModalUpdateUser";
import classnames from "classnames";
import config from "../../../config";

import "./Wrapper.css";

class Wrapper extends Component {
	constructor(props) {
		super();
		this.seconds = 5;
	}

	notificationSystem = React.createRef();

	handleNotify = (notification, data) => {
		let notificationSetup = {
			title: "Thông báo",
			message: data.notify,
			autoDismiss: 0,
			level: "info",
			position: "br",
			action: {
				label: "Xem ngay",
				callback: function () {},
			},
			uid: 1,
		};
		let notifyOff = false;
		switch (data.type) {
			case "message":
				if (this.props.location.pathname === "/messages") {
					notifyOff = true;
				} else {
					notificationSetup["action"]["callback"] = () =>
						this.props.actions.connectRoom({
							sendingId: data.sendingId,
						});
				}
				break;
			case "newFriend":
				if (this.props.location.pathname === "/friends") {
					notifyOff = true;
				} else {
					notificationSetup["action"] = {
						label: "Xem ngay",
						callback: () => <Redirect to="/friends" />,
					};
				}
				break;
			case "acceptFriend":
				if (this.props.location.pathname === "/friends") {
					notifyOff = true;
				} else {
					notificationSetup["action"]["callback"] = () =>
						this.props.actions.connectRoom({
							sendingId: data.sendingId,
						});
				}
				break;
			default:
				break;
		}
		!notifyOff && notification.addNotification(notificationSetup);
	};

	handleMessaging = () => {
		socket.on(MESSAGES.SERVER_SEND_ROOM_ID, (data) => {
			this.props.actions.getAllMessages({ roomId: data.roomId });
		});
		socket.on(MESSAGES.SERVER_SEND_MESSAGE, (data) => {
			globalAudio.play("audioNotify");
			this.props.actions.updateMessages(data);
		});
		socket.on(MESSAGES.SERVER_SEND_UPDATE_LIST_ROOM, (data) => {
			this.props.actions.updateRooms(data);
		});
		socket.on(MESSAGES.SERVER_SEND_TYPING, (data) => {
			this.props.actions.handleTyping(data);
		});
	};

	handleCalling = (notification) => {
		let answerSocketId;
		let answerWebRtcId;

		socket.on(MESSAGES.SERVER_SEND_WEB_RTC_NOT_RECEIVE_CALL, (data) => {
			//Người nhận huỷ cuộc gọi
			const { receiverId, receiverSocketId } = data;
			socket.emit(MESSAGES.CLIENT_SEND_WEB_RTC_CANCEL_CALL, {
				answerId: receiverId,
				answerSocketId: receiverSocketId,
			});
			this.props.actions.handleCancelCall();
		});
		socket.on(MESSAGES.SERVER_SEND_WEB_RTC_ME_NOT_RECEIVE_CALL, (data) => {
			const { receiverId, receiverSocketId } = data;
			socket.emit(MESSAGES.CLIENT_SEND_WEB_RTC_CANCEL_CALL, {
				answerId: receiverId,
				answerSocketId: receiverSocketId,
			});
			this.props.actions.handleCancelCall();
			this.props.actions.handleCalling({
				code: "close",
				response: {},
			});
		});
		socket.on(MESSAGES.SERVER_SEND_WEB_RTC_NOT_WANT_CALL, (data) => {
			//Huỷ cuộc gọi
			globalAudio.pause("audioCall");
			this.props.actions.handleCancelCall();
			this.props.actions.handleCalling({
				code: "close",
			});
		});
		socket.on(MESSAGES.SERVER_SEND_WEB_RTC_HAVING_A_CALL, (data) => {
			//Thông báo có cuộc gọi đến sẽ play nhạc và hiển thị modal thông báo cuộc gọi đến
			this.props.actions.handleConnectWebRtcSuccess(data);
			globalAudio.play("audioCall", true);
			this.props.actions.handleCalling({
				code: "isReceiver",
				response: data.response,
			});
		});
		socket.on(
			MESSAGES.SERVER_WEB_RTC_SEND_ANOTHER_CLIENT_RECEIVE_CALL,
			(data) => {
				this.props.actions.handleCancelCall();
			}
		);
		socket.on(MESSAGES.SERVER_WEB_RTC_SEND_ANSWER_RESPONSE, (data) => {
			const { success, response, message } = data;
			if (success) {
				const { peerId, receiverId } = response;
				answerSocketId = peerId;
				answerWebRtcId = receiverId;
				//Người gọi
				this.props.actions.handleCalling({
					code: "callInfo",
					response: { answerSocketId, answerWebRtcId },
				});
				globalAudio.pause("audioCall");
				this.props.actions.handleReceiverAcceptCall();
				window.open(
					`${config.apiURL}/video?isCaller=true&userId=${this.props.userInfo._id}&answerId=${answerWebRtcId}&&answerSocketId=${answerSocketId}&socketId=${socket.id}`,
					"Video call",
					"width=1000, height=600"
				);
			} else {
				notification.addNotification({
					title: "Thông báo",
					message,
					autoDismiss: 0,
					level: "error",
					position: "tr",
				});
			}
		});

		socket.on(MESSAGES.SERVER_WEB_RTC_SEND_CALL_RESPONSE, (data) => {
			const { success } = data;
			if (success) {
				this.props.actions.handleConnectWebRtcSuccess(data);
				this.props.actions.handleCalling({
					code: "isCaller",
					response: data.response,
				});
				globalAudio.play("audioCall", true);
				this.timer = setInterval(() => {
					socket.emit(MESSAGES.CLIENT_SEND_WEB_RTC_NOT_WANT_CALL, {
						userId: this.props.userInfo._id,
						answerId: this.props.answerWebRtcId,
					});
					this.props.actions.handleCancelCall();
					globalAudio.pause("audioCall");
					clearInterval(this.timer);
				}, 10000);
			} else {
				this.props.actions.handleConnectWebRtcFail(data);
			}
		});
	};

	handleMusic = (notification) => {
		socket.on(MESSAGES.SERVER_MUSIC_SEND_REQUEST_MUSIC_RESPONSE, (data) => {
			const { success, message } = data;

			const handleCancelRequest = () => {
				const {
					musicConnectId,
					musicUrl,
					userInfo,
					isCancelledRequestMusic,
				} = this.props;
				if (isCancelledRequestMusic) {
					socket.emit(
						MESSAGES.CLIENT_SEND_NOT_WANT_SEND_MUSIC_REQUEST,
						{
							musicConnectId,
							musicUrl,
							userId: userInfo._id,
						}
					);
				}
			};
			if (success) {
				notification.addNotification({
					title: "Music request",
					message: "Waiting for accept",
					autoDismiss: 0,
					level: "info",
					position: "tc",
					uid: "requester",
					onRemove: handleCancelRequest,
				});
			} else {
				notification.addNotification({
					title: "Thông báo",
					message,
					autoDismiss: 0,
					level: "error",
					position: "tr",
				});
			}
		});

		socket.on(MESSAGES.SERVER_MUSIC_SEND_HAVE_A_MUSIC_REQUEST, (data) => {
			const {
				musicConnectId,
				musicUrl,
				musicName,
				musicConnectSocketId,
			} = data;
			this.props.actions.handleCurSong(musicUrl);
			this.props.actions.handleProcessingMusic({
				code: "isListener",
				response: {
					musicConnectId,
					musicUrl,
					musicConnectSocketId,
					musicName,
				},
			});
			const { userInfo } = this.props;
			const handleAcceptRequestMusic = () => {
				this.props.actions.handleProcessingMusic({
					code: "playMusic",
				});
				socket.emit(MESSAGES.CLIENT_SEND_ACCEPT_MUSIC_REQUEST, {
					userId: userInfo._id,
					musicConnectId,
					musicConnectSocketId,
				});
			};

			const handleDenyRequestMusic = () => {
				socket.emit(MESSAGES.CLIENT_SEND_NOT_ACCEPT_MUSIC_REQUEST, {
					userId: userInfo._id,
					musicUrl,
					musicConnectId,
					musicConnectSocketId,
				});
			};

			notification.addNotification({
				title: "Music request",
				message: "Some one invite you to listen with them a song",
				autoDismiss: 0,
				level: "info",
				position: "tc",
				uid: "listener",
				onRemove: () => {
					const { isOnPlay, isCancelledRequestMusic } = this.props;
					if (
						isOnPlay === false &&
						isCancelledRequestMusic === false
					) {
						handleDenyRequestMusic();
					}
				},
				children: (
					<>
						<button
							className="button__music"
							onClick={handleAcceptRequestMusic}
						>
							<i
								className="fa fa-check text-success"
								aria-hidden="true"
							></i>
						</button>
						<button
							className="button__music"
							onClick={notification.removeNotification(
								"requester"
							)}
						>
							<i
								className="fa fa-times text-danger"
								aria-hidden="true"
							></i>
						</button>
					</>
				),
			});
		});

		socket.on(
			MESSAGES.SERVER_MUSIC_SEND_ACCEPT_MUSIC_REQUEST_RESPONSE,
			(data) => {
				//Người yêu cầu
				this.props.actions.handleProcessingMusic({
					code: "playMusic",
				});
				notification.removeNotification("requester");
			}
		);

		socket.on(MESSAGES.SERVER_MUSIC_SEND_NOT_WANT_ACCEPT, (data) => {
			this.props.actions.handleProcessingMusic({
				code: "stopSendRequest",
			});
			notification.removeNotification("requester");
			notification.addNotification({
				title: "Request Denied",
				message: "Receiver doesn't accept request",
				autoDismiss: 3,
				level: "info",
				position: "tc",
			});
		});

		socket.on(MESSAGES.SERVER_MUSIC_SEND_NOT_WANT_SEND_REQUEST, (data) => {
			notification.removeNotification("listener");
			this.props.actions.handleProcessingMusic({
				code: "stopSendRequest",
			});
		});

		socket.on(MESSAGES.SERVER_MUSIC_SEND_STOP_MUSIC, (data) => {
			this.props.actions.handleCurSong(null);
			this.props.actions.handleProcessingMusic({
				code: "stopMusic",
			});
		});

		socket.on(
			MESSAGES.SERVER_MUSIC_SEND_ANOTHER_ACCEPT_MUSIC_REQUEST,
			(data) => {
				notification.addNotification({
					title: "Already listening",
					message: "Receiver already in listening",
					autoDismiss: 2,
					level: "error",
					position: "tr",
				});
			}
		);
	};

	handleMusicWhenPageReload = () => {
		const {
			userInfo,
			isOnPlay,
			isRequester,
			musicUrl,
			musicConnectId,
			musicConnectSocketId,
		} = this.props;
		if (isOnPlay) {
			socket.emit(MESSAGES.CLIENT_SEND_STOP_MUSIC, {
				musicConnectId,
				musicUrl,
				userId: userInfo._id,
			});
		} else if (isRequester) {
			socket.emit(MESSAGES.CLIENT_SEND_NOT_WANT_SEND_MUSIC_REQUEST, {
				musicConnectId,
				musicUrl,
				userId: userInfo._id,
			});
		} else {
			console.log({
				userId: userInfo._id,
				musicUrl,
				musicConnectId,
				musicConnectSocketId,
			});
			socket.emit(MESSAGES.CLIENT_SEND_NOT_ACCEPT_MUSIC_REQUEST, {
				userId: userInfo._id,
				musicUrl,
				musicConnectId,
				musicConnectSocketId,
			});
		}
	};

	handleCallWhenPageReload = () => {
		const {
			userInfo,
			answerWebRtcId,
			answerSocketId,
			isReceiver,
			isOnCall,
		} = this.props;
		if (isReceiver) {
			socket.emit(MESSAGES.CLIENT_SEND_WEB_RTC_NOT_RECEIVE_CALL, {
				userId: userInfo._id,
				answerId: answerWebRtcId,
				answerSocketId,
			});
		} else if (isOnCall) {
			socket.emit(MESSAGES.CLIENT_SEND_WEB_RTC_CANCEL_CALL, {
				userId: userInfo._id,
				answerId: answerWebRtcId,
				answerSocketId,
			});
		} else {
			socket.emit(MESSAGES.CLIENT_SEND_WEB_RTC_NOT_WANT_CALL, {
				userId: userInfo._id,
				answerId: answerWebRtcId,
			});
		}
	};

	handlePageReload = () => {
		window.onbeforeunload = (e) => {
			console.log("reload===================");
			const { isOnUseMusic } = this.props;
			if (isOnUseMusic) {
				this.handleMusicWhenPageReload();
			} else {
				this.handleCallWhenPageReload();
				clearInterval(this.timer);
			}
		};
	};

	componentDidMount() {
		if (this.props.location.pathname !== "/") {
			this.props.actions.checkLogin(this.props.userInfo);
		}
		const notification = this.notificationSystem.current;

		socket.on(MESSAGES.CONNECT, (data) => {
			this.handleMessaging();
			this.handleCalling(notification);
			this.handleMusic(notification);
			socket.on(MESSAGES.SERVER_SEND_NOTIFY, (data) => {
				this.handleNotify(notification, data);
			});
			socket.on("Server-send-error", (data) => {
				console.log("error", data);
			});
		});
		this.handlePageReload();
	}

	componentDidUpdate(prevProps, prevState) {
		const { userInfo } = this.props;
		if (prevProps.userInfo !== userInfo && !_.isEmpty(userInfo)) {
			socket.emit(MESSAGES.CLIENT_SEND_SOCKET, { userId: userInfo._id });
		}
	}

	componentWillUnmount() {
		this.notificationSystem.current.clearNotifications();
		clearInterval(this.timer);
	}

	handleHangUp = () => {
		const { userInfo, answerSocketId, answerWebRtcId } = this.props;
		socket.emit(MESSAGES.CLIENT_SEND_WEB_RTC_CANCEL_CALL, {
			userId: userInfo._id,
			answerSocketId,
			answerId: answerWebRtcId,
		});
		this.props.actions.handleCalling({
			code: "close",
		});
	};

	handleStopMusic = () => {
		const { musicConnectId, musicUrl, userInfo } = this.props;
		this.props.actions.handleCurSong(null);
		socket.emit(MESSAGES.CLIENT_SEND_STOP_MUSIC, {
			musicConnectId,
			musicUrl,
			userId: userInfo._id,
		});
	};

	render() {
		const {
			// isOnCall,
			isOnPlay,
			isFullScreen,
			musicUrl,
			musicName,
		} = this.props;
		return (
			<React.Fragment>
				{this.props.location.pathname !== "/" ? (
					<div className="layout">
						<NavBar {...this.props} />
						{isOnPlay && (
							<Draggable bounds="parent" onStart={() => false}>
								<div className="scrolling_text__container">
									<p className="marquee">
										<span>
											{musicName}- {musicName}&nbsp;
										</span>
									</p>
									<div
										className="btn__audio"
										onClick={this.handleStopMusic}
									>
										<span>
											<i className="fa fa-times"></i>
										</span>
									</div>
									<audio
										autoPlay
										play="true"
										loop
										style={{ position: "absolute" }}
									>
										<source
											src={
												isOnPlay
													? `http://${musicUrl}`
													: ""
											}
										></source>
										Your brower does not support the audio
										tag
									</audio>
								</div>
							</Draggable>
						)}
						<Draggable bounds="parent">
							<div
								className={classnames("box rem-position-fix", {
									fullScreen: isFullScreen,
									visible_hidden: true,
								})}
							>
								<video
									id="videoStream"
									width="100%"
									height="100%"
									preload="none"
								></video>
								<div className="videoActions">
									<button
										type="button"
										className="btn btn-danger btn-floating btn-sm btn-video-cancel"
										onClick={this.handleHangUp}
									>
										<ReactSVG
											src={urlFormat(
												"/assets/images/f-close.svg"
											)}
										/>
									</button>
									&nbsp;
									<button
										type="button"
										className="btn btn-secondary btn-floating btn-sm btn-video-cancel"
										onClick={() =>
											this.props.actions.handleFullScreen()
										}
									>
										{isFullScreen ? (
											<i
												className="fa fa-compress"
												aria-hidden="true"
											></i>
										) : (
											<i
												className="fa fa-expand"
												aria-hidden="true"
											></i>
										)}
									</button>
								</div>
							</div>
						</Draggable>
						{this.props.children}
					</div>
				) : (
					this.props.children
				)}
				<NotificationSystem ref={this.notificationSystem} />
				<ModalUpdateUser {...this.props} />
			</React.Fragment>
		);
	}
}

export default Wrapper;
