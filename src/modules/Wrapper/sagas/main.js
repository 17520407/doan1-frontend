import { call, put } from "redux-saga/effects";
import { push } from "react-router-redux";
import { delay } from "redux-saga";
import * as actions from "../actions";
import * as friendActions from "../../FriendsPage/actions";
import * as messagesActions from "../../MessagesPage/actions";
import * as authAPI from "api/auth";
import * as roomAPI from "api/room";
import { takeAction } from "services/forkActionSagas";
import { get, save } from "services/localStoredService";
import _ from "lodash";
import { clearAll } from "store/storages/localStorage";

// Handle Check login
export function* handleCheckLogin(action) {
	try {
		let accessToken = get("accessToken");
		if (_.isEmpty(accessToken)) {
			yield put(push("/"));
			yield put(actions.checkLoginFail());
		} else {
			if (_.isEmpty(action.payload)) {
				yield put(actions.getUserInfo());
			}
			yield put(actions.checkLoginSuccess());
		}
	} catch (err) {
		yield put(actions.checkLoginFail(err));
	}
}

// Handle Get UserInfo
export function* handleGetUserInfo(action) {
	try {
		let res = yield call(authAPI.getUserInfo);
		switch (res.status) {
			case 200:
				save("userId", res.data.data._id);
				yield put(friendActions.getUserInfo(res.data.data));
				yield put(messagesActions.getUserInfo(res.data.data));
				yield put(actions.getUserInfoSuccess(res.data.data));
				break;
			default:
				break;
		}
	} catch (err) {
		yield put(actions.getUserInfoFail(err));
	}
}

export function* handleUpdateUserInfo(action) {
	try {
		let res = yield call(authAPI.updateUserInfo, action.payload);
		if (res.status === 200) {
			yield put(actions.updateUserInfoSuccess(res.data.data));
			yield put(friendActions.getUserInfo(res.data.data));
			yield put(messagesActions.getUserInfo(res.data.data));
		} else {
			yield put(actions.updateUserInfoFail(res));
		}
	} catch (err) {
		yield put(actions.updateUserInfoFail(err));
	}
}

// Handle Upload Image
export function* handleUploadImage(action) {
	try {
		let res = yield call(roomAPI.uploadImage, action.payload.file);
		yield delay(500);
		switch (res.status) {
			case 200:
				yield put(actions.uploadAvtSuccess(res.data.message));
				break;
			case 406:
				yield put(actions.uploadAvtFail(res.data));
				break;
			default:
				break;
		}
	} catch (err) {
		yield put(actions.uploadAvtFail(err));
	}
}

// Handle Logout
export function* handleLogOut(action) {
	try {
		clearAll();
		yield put(actions.checkLoginFail());
		yield put(actions.handleClear());
		yield put(push("/"));
	} catch (err) {
		yield put(actions.checkLoginFail(err));
	}
}

/*-----------------------------------------------------------------*/

export function* checkLogin() {
	yield takeAction(actions.checkLogin, handleCheckLogin);
}
export function* getUserInfo() {
	yield takeAction(actions.getUserInfo, handleGetUserInfo);
}
export function* logOut() {
	yield takeAction(actions.logOut, handleLogOut);
}
export function* updateUserInfo() {
	yield takeAction(actions.updateUserInfo, handleUpdateUserInfo);
}
export function* uploadAvt() {
	yield takeAction(actions.uploadAvt, handleUploadImage);
}
/*-----------------------------------------------------------------*/
export default [checkLogin, getUserInfo, updateUserInfo, uploadAvt, logOut];
