const MESSAGES = {
	CLIENT_SEND_SOCKET: "Client-send-socket",
	CLIENT_JOIN_ROOM: "Client-join-room",
	CLIENT_LEAVE_ROOM: "Client-leave-room",
	CLIENT_SEND_MESSAGE: "Client-send-message",
    CLIENT_SEND_FRIEND_REQUEST: "Client-send-friend-request",
    CLIENT_SEND_TYPING: "Client-send-typing",
    CLIENT_WEB_RTC_SEND_CALL_ID: "Client-webRtc-send-callId",
    CLIENT_WEB_RTC_SEND_ANSWER: "Client-webRtc-send-answer",
    CLIENT_SEND_WEB_RTC_CANCEL_CALL:"Client-webRtc-send-cancel-call",
    CLIENT_SEND_WEB_RTC_NOT_WANT_CALL:"Client-webRtc-send-not-want-call",
    CLIENT_SEND_WEB_RTC_NOT_RECEIVE_CALL:"Client-webRtc-send-not-receive-call",

    CLIENT_SEND_MUSIC_REQUEST:"Client-music-send-request-music",
    CLIENT_SEND_STOP_MUSIC:"Client-music-send-stop-music",
    CLIENT_SEND_ACCEPT_MUSIC_REQUEST:"Client-music-send-accept-music-request",
    CLIENT_SEND_NOT_WANT_SEND_MUSIC_REQUEST:"Client-music-send-not-want-send-request",
    CLIENT_SEND_NOT_ACCEPT_MUSIC_REQUEST:"Client-music-send-not-want-accept",
    // ==================================================================//
    SERVER_SEND_TYPING: "Server-send-typing",
	SERVER_SEND_UPDATE_LIST_ROOM: "Server-send-update-list-room",
	SERVER_SEND_MESSAGE: "Server-send-message",
	SERVER_SEND_NOTIFY: "Server-send-notify",
    SERVER_SEND_ROOM_ID: "Server-send-roomId",

    SERVER_WEB_RTC_SEND_CALL_RESPONSE: "Server-webRtc-send-call-response",
    SERVER_WEB_RTC_SEND_ANSWER_RESPONSE: "Server-webRtc-send-answer-response",
    SERVER_WEB_RTC_SEND_ANOTHER_CLIENT_RECEIVE_CALL: "Server-webRtc-send-another-client-receive-call",

    SERVER_SEND_WEB_RTC_ME_NOT_RECEIVE_CALL: "Server-send-webRtc-me-not-receive-call",
    SERVER_SEND_WEB_RTC_NOT_RECEIVE_CALL: "Server-send-webRtc-not-receive-call",
    SERVER_SEND_WEB_RTC_NOT_WANT_CALL: "Server-send-webRtc-not-want-call",
    SERVER_SEND_WEB_RTC_HAVING_A_CALL: "Server-send-webRtc-have-a-call",
    SERVER_SEND_WEB_RTC_CANCEL_CALL: "Server-send-webRtc-cancel-call",

    SERVER_MUSIC_SEND_REQUEST_MUSIC_RESPONSE: "Server-music-send-request-music-response",
    SERVER_MUSIC_SEND_ACCEPT_MUSIC_REQUEST_RESPONSE: "Server-music-send-accept-music-request-response",
    SERVER_MUSIC_SEND_HAVE_A_MUSIC_REQUEST: "Server-music-send-have-a-music-request",
    SERVER_MUSIC_SEND_NOT_WANT_ACCEPT_TO_ANOTHER_CLIENT: "Server-music-send-not-want-accept-to-another-client",
    SERVER_MUSIC_SEND_STOP_MUSIC: "Server-music-send-stop-music",
    SERVER_MUSIC_SEND_NOT_WANT_SEND_REQUEST: "Server-music-send-not-want-send-request",
    SERVER_MUSIC_SEND_NOT_WANT_ACCEPT: "Server-music-send-not-want-accept",
    SERVER_MUSIC_SEND_ANOTHER_ACCEPT_MUSIC_REQUEST: "Server-music-send-another-accept-music-request-want-accept",


	CONNECT: "connect",
	DISCONNECT: "disconnect",
};

export default MESSAGES;
