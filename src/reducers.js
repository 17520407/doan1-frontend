/**
 * @file reducers
 */

import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";

// Place for reducers' app
import Wrapper, { name as nameOfWrapper } from "modules/Wrapper";
import MessagesPage, { name as nameOfMessagesPage } from "modules/MessagesPage";
import AuthPage, { name as nameOfAuthPage } from "modules/AuthPage";
import FriendsPage, { name as nameOfFriendsPage } from "modules/FriendsPage";

const reducers = {
	[nameOfWrapper]: Wrapper,
	[nameOfMessagesPage]: MessagesPage,
	[nameOfAuthPage]: AuthPage,
	[nameOfFriendsPage]: FriendsPage,
};

const rootReducer = (history) =>
	combineReducers({
		...reducers,
		router: connectRouter(history),
	});

export default (history) => rootReducer(history);
