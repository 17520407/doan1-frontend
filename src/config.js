var lodash = require("lodash");

const base = {
	dateDisplay: "MMMM D, YYYY",
	dateFormat: "YYYY-MM-DD",
	datetimeFormat: "YYYY-MM-DD HH:mm:ss",
	lang: "vn",
	logging: "INFO", //level INFO/TRACE/DEBUG
};

const env = {
	development: {
		logging: "DEBUG",
		apiBaseURL: "https://backend.pdchatapp.tk/api",
        apiURL: "https://backend.pdchatapp.tk",
        peerConfig: {
			host: "webrtc.pdchatapp.tk",
			secure: true,
			port: 443,
			path: "webRtc",
			debug: 3,
		},
	},
	stage: {
		apiBaseURL: "https://backend.pdchatapp.tk/api",
        apiURL: "https://backend.pdchatapp.tk",
        peerConfig: {
			host: "webrtc.pdchatapp.tk",
			secure: true,
			port: 443,
			path: "webRtc",
			debug: 3,
		},
	},
	production: {
		logging: "DEBUG",
		apiBaseURL: "https://backend.pdchatapp.tk/api",
		apiURL: "https://backend.pdchatapp.tk",
		peerConfig: {
			host: "webrtc.pdchatapp.tk",
			secure: true,
			port: 443,
			path: "webRtc",
			debug: 3,
		},
	},
	onLocal: {
		apiBaseURL: "http://localhost:3540/api",
		apiURL: "http://localhost:3540",
        apiWebRtcURL: "http://localhost:3541",
        peerConfig: {
            host: "localhost",
            port: 3541,
            path: "/webRtc",
		},
	},
};

const envConfig = lodash.get(process.env, "REACT_APP_NODE_ENV", "stage");

const configsExport = { ...base, ...env[envConfig] };
const isDebug = configsExport.logging === "DEBUG" ? true : false;

export default {
	...configsExport,
	isDebug,
};

export const TITLE = "Chat app PD";
export const DESCRIPTION = "Chat app PD";
export const IMAGE = "";
